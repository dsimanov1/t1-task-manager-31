package ru.t1.simanov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.simanov.tm.api.repository.IUserRepository;
import ru.t1.simanov.tm.enumerated.Role;
import ru.t1.simanov.tm.model.User;

import java.util.Objects;

public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @NotNull
    public User create(
            @Nullable final String login,
            @Nullable final String password
    ) {
        @NotNull final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(password);
        user.setRole(Role.USUAL);
        return Objects.requireNonNull(add(user));
    }

    @NotNull
    public User create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email
    ) {
        @NotNull final User user = create(login, password);
        if (email != null) user.setEmail(email);
        return user;
    }

    @NotNull
    @Override
    public User create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final Role role
    ) {
        @NotNull final User user = create(login, password);
        if (role != null) user.setRole(role);
        return user;
    }

    @Nullable
    public User findByLogin(@Nullable final String login) {
        if (login == null) return null;
        return models
                .stream()
                .filter(m -> login.equals(m.getLogin()))
                .findFirst()
                .orElse(null);
    }

    @Nullable
    public User findByEmail(@Nullable final String email) {
        if (email == null) return null;
        return models
                .stream()
                .filter(m -> email.equals(m.getEmail()))
                .findFirst()
                .orElse(null);
    }

    @Override
    public Boolean isLoginExists(@Nullable final String login) {
        if (login == null) return null;
        return models
                .stream()
                .anyMatch(m -> login.equals(m.getLogin()));
    }

    @Override
    public Boolean isEmailExists(@Nullable final String email) {
        if (email == null) return null;
        return models
                .stream()
                .anyMatch(m -> email.equals(m.getEmail()));
    }

}
